function myOuterFunction()
{
	var str = "Outer function";
	document.getElementById("h1").innerHTML = str;
}

function tryObject()
{
	var obj1 = {name:"obj1", value:1};
	document.getElementById("obj").innerHTML = obj1.name;
}

function tryGlobalVariable()
{
	var gVar = "l";
	document.getElementById("globalVar").innerHTML = gVar;
}

function tryGlobalVariable2()
{
	document.getElementById("globalVar").innerHTML = gVar;
}